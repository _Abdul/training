import React, { useState, useEffect } from 'react';
import ButtonSpinner from '../common/ButtonSpinner';
import isEmpty from '../utils/isEmpty';
import Flatpickr from "react-flatpickr";
import moment from 'moment';
import Maps from '../common/Maps';
import { useParams, useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { InputGroup, FormControl } from 'react-bootstrap';
import { validator } from 'javascript-input-validator';
import { getSinglePost, createPost, editPost } from '../redux/posts/postActions';
import "flatpickr/dist/themes/material_green.css";

const CreateEditPosts = () => {

  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();

  const { buttonLoading, loading } = useSelector(state => state?.loading);
  const { single_post } = useSelector(state => state.posts);

  const [errors, setErrors] = useState({});

  const [shown, setShown] = useState(true);
  const [isDataGet, setIsDataGet] = useState(false);

  const [date, setDate] = useState(moment().format('YYYY-MM-DD'));

  const [formData, setFormData] = useState({
    text: '',
    name: '',
  });
  const { name, text } = formData;

  useEffect(() => {
    if (id !== 'new' && shown) {
      dispatch(getSinglePost(id));
      setShown(false)
    }
    if (id !== 'new' && !isEmpty(single_post) && !isDataGet) {
      setFormData({
        text: single_post.text,
        name: single_post.user,
      });
      setDate(moment(single_post.date).format('YYYY-MM-DD'));
      setIsDataGet(true);
    }
  }, [id, dispatch, shown, isDataGet, single_post])

  const validate = () => {
    let feildSchema = [
      { name: 'name', value: name, required: true },
      { name: 'text', value: text, required: true },
      { name: 'date', value: date, required: true }
    ]
    return validator(feildSchema);
  }

  const handleSubmit = e => {
    e.preventDefault();
    const errors = validate();
    if (!errors) {
      const data = {
        text
      };
      dispatch(id === 'new' ? createPost(data, history) : editPost(id, data, history));
    }
    else {
      setErrors(errors)
    }
  }

  return (
    <div className="card">
      <div className="card-header">
        <h3>{id === 'new' ? 'Create Post' : 'View Post'}</h3>
      </div>
      <div className="card-body col-6">
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Post</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Please enter text..."
            value={text}
            className={`form-control ${errors.text && 'is-invalid'}`}
            onChange={e => setFormData({ ...formData, text: e.target.value })}
          />
          {errors.text &&
            <div className="invalid-feedback"> {errors.text}</div>
          }
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">User Name</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Please enter name..."
            value={name}
            className={`form-control ${errors.text && 'is-invalid'}`}
            onChange={e => setFormData({ ...formData, name: e.target.value })}
          />
          {errors.name &&
            <div className="invalid-feedback"> {errors.name}</div>
          }
        </InputGroup>
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Date</InputGroup.Text>
          </InputGroup.Prepend>
          <Flatpickr
            value={date}
            className={`form-control ${errors.date && 'is-invalid'} bg-white`}
            onChange={date => setDate(moment(date[0]).format('YYYY-MM-DD'))}
          />
          {errors.date &&
            <div className="invalid-feedback"> {errors.date}</div>
          }
        </InputGroup>
      </div>
      <Maps />
      <div className="card-footer text-muted">
        {id === 'new' &&
          <button type="submit" className="btn btn-primary mr-2" style={{ minWidth: '74px' }} disabled={loading} onClick={handleSubmit}>
            {buttonLoading ? <ButtonSpinner variant="light" /> : 'Create'}
          </button>
        }
        <button type="submit" className={`btn btn-${id === 'new' ? 'danger' : 'primary'}`} onClick={() => history.push('/posts')}>{id === 'new' ? 'Cancel' : 'Back'}</button>
      </div>
    </div>
  )
}
export default CreateEditPosts;