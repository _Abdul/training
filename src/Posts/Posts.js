import React, { useEffect, useState } from 'react';
import isEmpty from '../utils/isEmpty';
import moment from 'moment';
import ConfirmModal from '../common/ConfirmModal';
import { Dropdown, Button } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux'
import { getAllPosts, deletePost } from '../redux/posts/postActions';
import { CLEAR_POSTS } from '../redux/types';
import { MDBDataTable } from 'mdbreact';
import ReactDragListView from 'react-drag-listview/lib/index.js';

const Posts = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { loading } = useSelector(state => state.loading);
  const { all_posts } = useSelector(state => state.posts);
  const [formData, setFormData] = useState({ show: false, id: '' })
  const { id, show } = formData;
  const [isDataGet, setIsDataGet] = useState(false);
  const [shown, setShown] = useState(true);

  const columns = [
    {
      label: '#',
      field: '_id',
      width: 150,
      sort: 'asc',
      attributes: {
        'aria-controls': 'DataTable',
        'aria-label': 'Name',
      },
    },
    {
      label: 'User',
      field: 'user',
      width: 270,
    },
    {
      label: 'Text',
      field: 'text',
      width: 200,
    },
    {
      label: 'Date',
      field: 'date',
      width: 150,
    },
    {
      label: 'Action',
      field: 'actions',
      sort: 'disabled',
      width: 100,
      searchable: 'false'
    },
  ];

  const [data, setData] = useState({
    columns,
    rows: []
  });

  useEffect(() => {
    if (shown) {
      dispatch(getAllPosts());
      setShown(false);
    }
    if (!isDataGet && !isEmpty(all_posts)) {
      setData({
        ...data,
        rows: all_posts.map(post => {
          return {
            _id: post._id,
            user: post.user,
            text: post.text,
            date: moment(post.date).format('YYYY-MM-DD'),
            actions:
              <Dropdown>
                <Dropdown.Toggle variant="dark" id="dropdown-basic">
                  Actions
              </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={() => history.push(`/posts/${post._id}`)}>View</Dropdown.Item>
                  <Dropdown.Item onClick={() => handleDelete(post._id)}>Delete</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
          };
        })
      });
      setIsDataGet(true)
    }
  }, [dispatch, shown, isDataGet, all_posts, history, data]);

  useEffect(() => {
    return () => {
      dispatch({
        type: CLEAR_POSTS
      })
    }
  }, [dispatch]);

  const handleDelete = id => {
    setFormData({ id, show: true });
  }

  const handleModalSubmit = () => {
    dispatch(deletePost(id));
    handleModalClose();
  }

  const handleModalClose = () => {
    setFormData({ id: '', show: false });
  }

  const dragProps = {
    onDragEnd(fromIndex, toIndex) {
      const tableData = [...data.rows];
      const item = tableData.splice(fromIndex, 1)[0];
      tableData.splice(toIndex, 0, item);
      setData({...data,rows : tableData})
    },
    nodeSelector: 'tr',
    handleSelector: 'tr'
  };
  return (
    <div className="pt-4">
      <Button variant="dark" className="mb-4 text-white" onClick={() => history.push("/posts/new")}>
        Create Post
      </Button>
      <ReactDragListView {...dragProps}>
        <MDBDataTable
          hover
          entriesOptions={[10, 25, 50]}
          data={data}
          noRecordsFoundLabel={loading ? 'Loading...' : 'No posts found'}
        >
          
        </MDBDataTable>
      </ReactDragListView>
      <ConfirmModal
        show={show}
        handleClose={handleModalClose}
        handleDelete={handleModalSubmit}
      />
    </div>
  )
}
export default Posts;