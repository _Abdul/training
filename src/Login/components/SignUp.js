import React, { useState } from 'react';
import ButtonSpinner from '../../common/ButtonSpinner';
import { useHistory } from 'react-router';
import { validator } from 'javascript-input-validator';
import { useDispatch, useSelector } from 'react-redux';
import { signUp } from '../../redux/auth/authAction';
import '../css/login.css';

const SignUp = () => {

  const history = useHistory();
  const dispatch = useDispatch();

  const [formData, setFormData] = useState({ name: '', email: '', password: '' });
  const { name, email, password } = formData;
  const [errors, setErrors] = useState({});

  const { buttonLoading, loading } = useSelector(state => state?.loading);

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  }

  const validate = () => {
    let feildSchema = [
      { name: 'name', value: name, required: true },
      { name: 'email', value: email, isEmail: true, required: true },
      { name: 'password', value: password, min: 6, required: true },
    ]
    return validator(feildSchema);
  }

  const handleSubmit = e => {
    e.preventDefault();
    const errors = validate();
    if (!errors) {
      dispatch(signUp(formData, history));
      setErrors({});
    }
    else {
      setErrors(errors)
    }
  }

  return (
    <>
      <div className="sidenav">
        <div className="login-main-text">
          <h2>Application<br /> Login Page</h2>
          <p>Login or register from here to access.</p>
        </div>
      </div>
      <div className="main">
        <div className="col-md-6 col-sm-12">
          <div className="login-form">
            <form>
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  className={`form-control ${errors.name && 'is-invalid'}`}
                  name="name"
                  placeholder="Name"
                  value={name}
                  onChange={handleChange}
                />
                {errors.name &&
                  <div className="invalid-feedback"> {errors.name}</div>
                }
              </div>
              <div className="form-group">
                <label>Email</label>
                <input
                  type="text"
                  className={`form-control ${errors.email && 'is-invalid'}`}
                  name="email"
                  placeholder="Email"
                  value={email}
                  onChange={handleChange}
                />
                {errors.email &&
                  <div className="invalid-feedback"> {errors.email}</div>
                }
              </div>
              <div className="form-group">
                <label>Password</label>
                <input
                  type="password"
                  className={`form-control ${errors.password && 'is-invalid'}`}
                  name="password"
                  placeholder="Password"
                  value={password}
                  onChange={handleChange}
                />
                {errors.password &&
                  <div className="invalid-feedback"> {errors.password}</div>
                }
              </div>
              <button type="submit" className="btn btn-black mr-3 text-white" onClick={handleSubmit} style={{ minWidth: '84px' }}>
                {buttonLoading ? <ButtonSpinner variant="light" /> : 'Sign Up'}
              </button>
              <button type="submit" className="btn btn-secondary" onClick={() => history.push('/login')} disabled={loading}>
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  )
}

export default SignUp
