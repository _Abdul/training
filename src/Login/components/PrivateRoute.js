import React from 'react';
import { Route, Redirect, useLocation } from 'react-router-dom';
import { getCurrentUser } from '../../redux/auth/authAction';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const location = useLocation();

  return (
    <Route {...rest} render={props => (
      getCurrentUser() ? <Component {...props} /> :
        <Redirect to={{ pathname: '/login', state: { from: location.pathname } }} />
    )} />
  )
}

export default PrivateRoute
