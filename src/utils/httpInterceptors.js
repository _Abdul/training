import { userAxios } from "./config";
import store from '../redux/store';
import { getError, clearErrors } from '../redux/error/errorAction';
import { START_LOADING, STOP_LOADING, START_BUTTON_LOADING, STOP_BUTTON_LOADING } from "../redux/types";
import { toast } from 'react-toastify';

const { dispatch } = store;

//Add a request interceptor
userAxios.interceptors.request.use(config => {
  const token = localStorage.getItem("token") && JSON.parse(localStorage.getItem("token"));
  let method = config.method;
  dispatch(startLoading());

  if (token) {
    config.headers.common['AUTHORIZATION'] = `${token}`;
  }
  
  if (method === 'post' || method === 'put' || method === 'patch') {
    dispatch(startButtonLoader());
  }
  return config;
}, error => {
  dispatch(getError(error));
  return Promise.reject(error);
});

//Add a response interceptor
userAxios.interceptors.response.use(response => {
  dispatch(clearErrors());
  dispatch(stopLoading());
  dispatch(stopButtonLoader());
  return response;
}, error => {
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;
  if (!expectedError) {
    toast.error("An unexpected error occured!");
  } else {
    dispatch(getError(error));
  }
  dispatch(stopLoading());
  dispatch(stopButtonLoader());
  dispatch(clearErrors());
  return Promise.reject(error);
});

//Start Loading
export const startLoading = () => {
  return {
    type: START_LOADING
  };
};

//Stop Loading
export const stopLoading = () => {
  return {
    type: STOP_LOADING
  };
};

//Start Loader
export const startButtonLoader = () => {
  return {
    type: START_BUTTON_LOADING
  };
};

//Start Loading
export const stopButtonLoader = () => {
  return {
    type: STOP_BUTTON_LOADING
  };
};