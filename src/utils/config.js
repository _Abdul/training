import axios from 'axios';

const userAxios = axios.create({
  baseURL : 'http://ea59b3f4bf8e.ngrok.io/api/v1'
});

export { 
  userAxios
};