import React, { useState } from 'react';
import Result from './Result';
import KeyPad from './KeyPad';
import './calculator.css' 

const Calculator = () => {
  const [result, setResult] = useState('');

  const onClick = button => {

    if (button === "=") {
      calculate()
    }

    else if (button === "C") {
      reset()
    }
    else if (button === "CE") {
      backspace();
    }

    else {
      setResult(result + button)
    }
  };

  const backspace = () => {
    setResult(result.slice(0, -1));
  }

  const reset = () => {
    setResult('');
  }

  const calculate = () => {
    try {
      let data = result.replace(/^0+/, '');
      // eslint-disable-next-line
      let finalResult =  eval((data) || "") + "";

      setResult(finalResult === 'Infinity' ? 'Number not found' : finalResult)
    } catch (e) {
      setResult('error')
    }
  }

  const handleChange = e => {
    let value = e.target.value;
    setResult(value)
  }

  return (
    <div className="calculator-body">
      <h1 className="mb-4">Simple Calculator</h1>
      <Result result={result} handleChange={handleChange} />
      <KeyPad onClick={onClick} />
    </div>
  )
}
export default Calculator;
