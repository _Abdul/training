import React from 'react';

const KeyPad = ({ onClick }) => {
  const buttons = [
    {name: "%", class:"upper3"},
    {name: "/", class:"upper3", text:'÷'},
    {name: "CE", class:"upper3"},
    {name: "C", class:"special", text:'Clear'},

    {name: "1"},
    {name: "2"},
    {name: "3"},
    {name: "+", class:"special"},

    {name: "4"},
    {name: "5"},
    {name: "6"},
    {name: "-", class:"special"},

    {name: "7"},
    {name: "8"},
    {name: "9"},
    {name: "*", class:"special", text:'x'},

    {name: "0", class:'custom-bnt-w'},
    {name: "."},
    {name: "=", class:"special"},

  ]
  return (
    <div className="button">
      {buttons.map((data, index) => 
        <button name={data.name} className={data.class} onClick={e => onClick(e.target.name)} key={index}>
          {data?.text ? data?.text : data?.name}
        </button>
      )}

    </div>
  )
}
export default KeyPad;
