import React from 'react';

const Result = ({ result, handleChange }) => {
  const keyCodes = [48,49,50,51,52,53,54,55,56,57,187,189,191,8];

  const keyCheck = e => {
    const { keyCode, shiftKey } = e;
    if (!keyCodes.includes(keyCode)) {
      return e.preventDefault();
    }
    if (keyCodes.includes(keyCode)) {
      if (((keyCode === 48) || (keyCode === 49) || (keyCode === 50) || (keyCode === 51) ||
        (keyCode === 52) || (keyCode === 54) || (keyCode === 55) || (keyCode === 57) || (keyCode === 187)
        || (keyCode === 189) || (keyCode === 191)) && shiftKey) {
        e.preventDefault();
      }
    }
  }
  return (
    <div className="result">
      <input type="text" autoFocus value={result} onKeyDown={keyCheck} onChange={handleChange} />
    </div>
  )
}
export default Result;
