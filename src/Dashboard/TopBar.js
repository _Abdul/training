import React from 'react';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { logout } from '../redux/auth/authAction';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';

const TopBar = () => {
  const history = useHistory();
  const { url } = useRouteMatch();

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Navbar.Brand role="button">React-Bootstrap</Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto nav-links">
          <Link to="/dashboard" className={`text-white mr-3 text-decoration-none ${url === '/dashboard' && 'active'}`} >Dashboard</Link>
          <Link to="/posts" className={`text-white mr-3 text-decoration-none ${url === '/posts' && 'active'}`}>Posts</Link>
          <Link to="/calculator" className={`text-white mr-3 text-decoration-none ${url === '/calculator' && 'active'}`}>Calculator</Link>
          <Link to="/webcam" className={`text-white text-decoration-none ${url === '/webcam' && 'active'}`}>Webcam</Link>
        </Nav>
        <Nav>
          <Button variant="light" className="nav-bar-btn mr-3" onClick={() => history.push('/profile/new')}>
            <FontAwesomeIcon icon={faUserPlus} /> Profile
          </Button>
          <Button variant="light" className="nav-bar-btn" onClick={() => logout(history)}>
            Logout
          </Button>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}
export default TopBar;
