import React from 'react';
import Posts from '../posts/Posts';
import CreateEditPosts from '../posts/CreateEditPosts';
import Calculator from '../calculator/Calculator';
import CreateEditProfile from '../profiles/CreateEditProfile';
import { Route, Switch, Redirect } from 'react-router';
import { getCurrentUser } from '../redux/auth/authAction';
import WebCam from '../common/WebCam';

const DashboardRoutes = () => {

  if (!getCurrentUser()) {
    return <Redirect to="/login" />
  }

  return (
    <Switch>
      <Route exact path="/dashboard" component={() => <h1>Welcome to Dashboard</h1>} />

      <Route exact path="/posts" component={Posts} />
      <Route exact path="/posts/:id" component={CreateEditPosts} />

      <Route exact path="/profile/:id" component={CreateEditProfile} />

      <Route exact path="/calculator" component={Calculator} />

      <Route exact path="/webcam" component={WebCam} />

      <Redirect to="/dashboard" />
    </Switch>
  )
}
export default DashboardRoutes;
