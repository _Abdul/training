import React from 'react';
import MainContent from './MainContent';
import TopBar from './TopBar';

const Dashboard = () => {
  return (
    <>
    <TopBar/>
    <MainContent/>
    </>
  )
}
export default Dashboard;
