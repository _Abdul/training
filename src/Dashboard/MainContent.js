import React from 'react';
import DashboardRoutes from './DashboardRoutes';

const MainContent = () => {
  return (
    <React.Fragment>
      <div className="container ml-auto mr-auto mt-4 ">
        <DashboardRoutes />
      </div>
    </React.Fragment>
  )
}
export default MainContent;
