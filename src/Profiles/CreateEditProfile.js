import React, { useState } from 'react';
import Select from 'react-select';
import ButtonSpinner from '../common/ButtonSpinner';
import { InputGroup, FormControl, Form } from 'react-bootstrap';
import { validator } from 'javascript-input-validator';
import { useParams, useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { createProfile } from '../redux/posts/postActions';

const CreateEditProfile = () => {

  const history = useHistory();
  const dispatch = useDispatch();
  const [errors, setErrors] = useState({});
  const { id } = useParams();

  const { buttonLoading, loading } = useSelector(state => state?.loading);

  const options = [
    { name: 'Please Select', value: '' },
    { name: 'Active', value: 'Active' },
    { name: 'InActive', value: 'inActive' }
  ];

  const secondOptions = [
    { value: 'JS', label: 'Javascript' },
    { value: 'Node js', label: 'Node' },
    { value: 'Vanilla js', label: 'Vanilla js' },
    { value: 'React js', label: 'React' },
    { value: 'Vue js', label: 'Vue' },
    { value: 'Angular js', label: 'Angular' },
  ];

  const [formData, setFormData] = useState({ status: '', handle: '' });
  const { status, handle } = formData;
  const [selectedOption, setSelectedOption] = useState([]);

  const handleChange = selectedOption => {
    setSelectedOption(selectedOption);
  }
  const validate = () => {
    let feildSchema = [
      { name: 'name', label: 'Name', value: handle, min: 10, required: true },
      { name: 'status', value: status, messageString: 'Please select at least one status', required: true },
      { name: 'selected', value: selectedOption, messageString: 'Please select at least one skill', required: true }
    ]
    return validator(feildSchema);
  }

  const handleSubmit = e => {
    e.preventDefault();
    const errors = validate();
    if (!errors) {
      let skills = selectedOption.map(data => data.value);
      skills = skills.toString();
      const data = {
        handle,
        status,
        skills
      }
      dispatch(createProfile(data, history));
      setErrors({});
    }
    else {
      setErrors(errors);
    }
  }

  return (
    <div className="card">
      <div className="card-header">
        <h3>{id === 'new' ? 'Create Profile' : 'Edit Profile'}</h3>
      </div>
      <div className="card-body col-6">
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Training</InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            placeholder="Please enter text..."
            value={handle}
            className={`form-control ${errors.name && 'is-invalid'}`}
            onChange={e => setFormData({ ...formData, handle: e.target.value })}
          />
          {errors.name &&
            <div className="invalid-feedback"> {errors.name}</div>
          }
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Status</InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            as="select"
            value={status}
            className={`${errors.status && 'is-invalid'}`}
            onChange={e => setFormData({ ...formData, status: e.target.value })}
          >
            {options.map((data, index) => (
              <option value={data.value} key={index}> {data.name}</option>
            ))}
          </Form.Control>
          {errors.status &&
            <div className="invalid-feedback"> {errors.status}</div>
          }
        </InputGroup>

        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">Skills</InputGroup.Text>
          </InputGroup.Prepend>
          <Select
            value={selectedOption}
            onChange={handleChange}
            options={secondOptions}
            isMulti={true}
            className={`min-wid ${errors.selected && 'is-invalid'}`}
          />
          {errors.selected &&
            <div className="invalid-feedback"> {errors.selected}</div>
          }
        </InputGroup>


      </div>
      <div className="card-footer text-muted">
        {id === 'new' &&
          <button type="submit" className="btn btn-primary mr-2" style={{ minWidth: '74px' }} disabled={loading} onClick={handleSubmit}>
            {buttonLoading ? <ButtonSpinner variant="light" /> : (id === 'new' ? 'Create' : 'Edit')}
          </button>
        }
        <button type="submit" className='btn btn-danger' onClick={() => history.push('/posts')}>Cancel</button>
      </div>
    </div>
  )
}
export default CreateEditProfile;