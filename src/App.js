import React from 'react';
import store from './redux/store';
import Login from './login/components/Login';
import SignUp from './login/components/SignUp';
import PrivateRoute from './login/components/PrivateRoute';
import Dashboard from './dashboard/Dashboard';
import { Provider } from 'react-redux';
import { Switch, Route, Redirect } from 'react-router';
import './App.css';

const App = () => {
  return (
    <Provider store={store}>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={SignUp} />
        <PrivateRoute path="/:page?" component={Dashboard} />
        <Redirect exact from="/" to="/:page?" />
      </Switch>
    </Provider>
  );
}
export default App;
