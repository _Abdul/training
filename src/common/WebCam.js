import React, { useRef, useState } from 'react';
import Webcam from 'react-webcam';
import Cropper from './Cropper';
import { MDBRow, MDBBtn, MDBCard, MDBCardBody, MDBCol, MDBContainer } from 'mdbreact';

const WebCam = () => {
  const webcamRef = useRef(null);
  const [imgSrc, setImgSrc] = useState('');

  const capture = () => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImgSrc(imageSrc)
  }
  const cancel = () => {
    setImgSrc('');
  }

  return (
    <>
      <MDBContainer className="video-cam">
        <MDBRow>
          <MDBCol size="8">
            <MDBCard >
              <MDBCardBody>
                <Webcam
                  audio={false}
                  ref={webcamRef}
                  screenshotFormat="image/jpeg"
                />
                <MDBBtn onClick={capture}>Capture photo</MDBBtn>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol size="4">
            {imgSrc && <Cropper image={imgSrc} cancel={cancel} />}
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </>

  )
}
export default WebCam;
