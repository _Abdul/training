import React from 'react';
import { Spinner } from 'react-bootstrap';

const ButtonSpinner = ({ variant }) => {
  return (
    <Spinner animation="border" variant={variant} size="sm" >
      <span className="sr-only">Loading...</span>
    </Spinner>
  )
}
export default ButtonSpinner;
