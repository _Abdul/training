import React from 'react';
import GoogleMapReact from 'google-map-react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

const Maps = () => {
  const position = {
    center: {
      lat: 31.475413,
      lng: 74.306187
    },
    zoom: 16
  }
  return (
    <div style={{ height: '100vh', width: '100%', padding:'5px' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: '' }}
        defaultCenter={position.center}
        defaultZoom={position.zoom}
      >
        <AnyReactComponent
          lat={31.475413}
          lng={74.306187}
          text="Office"
        />
      </GoogleMapReact>
    </div> 
  )
}
export default Maps

const AnyReactComponent = ({ text }) => 
  <div style={{
    position: 'absolute',
    transform: 'translate(-50%, -50%)'
  }}>
  <FontAwesomeIcon icon={faMapMarkerAlt } className="fa-3x" /> {text}
</div>;
