import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const ConfirmModal = ({show, handleDelete, handleClose}) => {
  return (
    <Modal show={show}>
      <Modal.Header closeButton>
        <Modal.Title>Modal heading</Modal.Title>
      </Modal.Header>
      <Modal.Body>Are you sure you want to delete!</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
      </Button>
        <Button variant="primary" onClick={handleDelete}>
          Save Changes
      </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default ConfirmModal;
