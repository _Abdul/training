import { GET_ALL_POSTS, GET_SINGLE_POST, DELETE_SINGLE_POST, CLEAR_POSTS } from '../types';

const initialState = {
  all_posts : [],
  single_post : null
}
export default function( state = initialState, action){
  switch (action.type) {
    case GET_ALL_POSTS:
      return{
        ...state,
        all_posts : action.payload,
        single_post : null
      }
    case GET_SINGLE_POST:
      return {
        ...state,
        single_post: action.payload
      }
    case DELETE_SINGLE_POST:
      return {
        ...state,
        all_posts: state.all_posts.filter(item => item.id !== action.payload)
      }
    case CLEAR_POSTS:
      return {
        all_posts: [],
        single_post: null
      }
    default:
      return state;
  }
};
