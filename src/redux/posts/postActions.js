// import { userAxios } from "../../../utils/config" ;
import { GET_ALL_POSTS, DELETE_SINGLE_POST, GET_SINGLE_POST } from "../types";
import { toast } from 'react-toastify';
import { userAxios } from "../../utils/config";

export const getAllPosts = () => dispatch => {
  userAxios
    .get('/posts')
    .then(res => {
      dispatch({
        type: GET_ALL_POSTS,
        payload: res.data
      });
    })
    .catch(() => toast.error('Oops unable to get posts!'))
};

export const getSinglePost = id => dispatch => {
  userAxios
    .get(`/posts/${id}`)
    .then(res => {
      dispatch({
        type: GET_SINGLE_POST,
        payload: res.data
      })
    })
    .catch(err => console.log(err.response))
};

export const deletePost = id => dispatch => {
  userAxios
    .delete(`/posts/${id}`)
    .then(() => {
      toast.succes('Post deleted successfully.', {autoClose: 2900})
      dispatch({
        type: DELETE_SINGLE_POST,
        payload: id
      });
    })
    .catch(() => toast.error('Oops unable to delete post!'))
};

export const createPost = (data, history) => () => {
  userAxios
    .post(`/posts`, data)
    .then(() => {
      toast.success('Post created successfully.');
      history.push('/posts');
    })
    .catch(err => {
      if (err?.response?.status === 400) {
        toast.error(err?.response?.data?.text)
      }
    })
};

export const editPost = (id, data, history) => () => {
  userAxios
    .put(`/posts/${id}`, data)
    .then(() => {
      toast.succes('Post updated successfully.', {autoClose: 2900})
      history.push('/posts');
    })
    .catch(err => console.log(err.response))
};

export const createProfile = (data, history) => () => {
  userAxios
    .post(`/profile`, data)
    .then(res => {
      toast.success('Profile created successfully.');
      history.push('/posts');
    })
    .catch(err => console.log(err.response))
};
