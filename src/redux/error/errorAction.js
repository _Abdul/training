import { GET_ERRORS, CLEAR_ERRORS } from '../types';
import { logout } from '../auth/authAction';

//Get Errors
export const getError = error => dispatch => {
  if (error.response) {
    if (error.response.status === 401) {
      dispatch(logout());
    }
    if (error.response.status !== 200) {
      dispatch({
        type: GET_ERRORS,
        payload: error.response.data
      });
    }
  }
};

//Clear Errors
export const clearErrors = () => dispatch => {
  dispatch({
    type: CLEAR_ERRORS
  });
};