import { START_LOADING, STOP_LOADING, START_BUTTON_LOADING, STOP_BUTTON_LOADING } from '../types';

const initalState = {
  loading : false,
  buttonLoading : false
}

export default function(state = initalState, action){
  switch (action.type) {
    case START_LOADING:
      return{
        ...state,
        loading : true
      }
    case STOP_LOADING:
      return {
        ...state,
        loading: false
      }
    case START_BUTTON_LOADING:
      return {
        ...state,
        buttonLoading: true
      }
    case STOP_BUTTON_LOADING:
      return {
        ...state,
        buttonLoading: false
      }
    default:
      return state
  }
}