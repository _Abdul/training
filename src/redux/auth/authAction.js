import { userAxios } from '../../utils/config';
import isEmpty from '../../utils/isEmpty';
import { toast } from 'react-toastify';

export const login = (data, history) => () => {

  userAxios
  .post('/users/login', data)
  .then(res => {
    toast.success('Logged in successfully.');  
    let token = res.data.token;
    localStorage.setItem('token', JSON.stringify(token));
    history.push(history?.location?.state?.from ? history?.location?.state?.from : '/dashboard');
  })
  .catch(err => {
    if (err?.response?.status === 404) {
      toast.error(err?.response?.data?.email)
    }
    if (err?.response?.status === 400) {
      toast.error(err?.response?.data?.password)
    }
  })
}

export const signUp = (data, history) => () => {
  userAxios
  .post('/users/signup', data)
  .then(() => {
    toast.success('User created successfully');
    setTimeout(() => history.push('/login'), 300);
  })
  .catch(err => {
    if (err?.response?.status === 400) {
      toast.error(err?.response?.data?.email);
    }
  })
}
  
export const getCurrentUser = () => {
  const token = JSON.parse(localStorage.getItem('token'));
  return isEmpty(token) ? null : token;
};

export const logout = history => {
  localStorage.removeItem('token');
  history.push('/login');
}