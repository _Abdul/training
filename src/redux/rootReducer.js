import { combineReducers } from 'redux';
import loadingReducer from './loading/loadingReducer';
import errorReducer from './error/errorReducer';
import postReducer from './posts/postReducer';

const rootReducer = combineReducers({
  loading: loadingReducer,
  error : errorReducer,
  posts: postReducer
})

export default rootReducer;